# news-crawler-web
Tools and Dashboard for [news-crawler](https://gitlab.com/abocado/news-crawler)

## Requirements
- [news-crawler](https://gitlab.com/abocado/news-crawler) and it's requirements
- PHP 7+

## Getting started
`git clone https://gitlab.com/news-crawler1/news-crawler-web.git`  

Create VirtualHosts in Apache for /public and/or /dashboard  

Copy `cred.example.php` to `cred.php`
- fill in your database credentials 
- and the user - password combination that is used to login into you dashboard and for news-checker (ajax_user and ajax_pw)


# Dashboard

## Status
![Status Image](/readme/status.png)  

For the Crawler Status to show, you need to run news-crawler with parameter -j 
and it must be on the same server (communication via localhost)

## Log
![Log Image](/readme/log.png)  

You can filter the log by selecting a level from the dropdown or typing text
into the table header fields


## Statistics & warning thresholds
![Statistics Image](/readme/statistics.png)  
![Detail Image](/readme/statistics_detail.png)  

In the big statistics table you can click any column to change the thresholds 
and get a list of items, that are relevant to that parameter.  
You can also hover over any field to see the thresholds.

## Graphs
![Graph Image 1](/readme/graph1.png)  
![Graph Image 2](/readme/graph2.png)  
![Graph Image 3](/readme/graph3.png)  

Graphs are interactive. You can hover over the dots for more info 
and hide datasets by click on them in the legend
