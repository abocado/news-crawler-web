class crawlerGraphs
{
    constructor () 
    {
        
        load('crawl_percent', this.populateCrawlPercent.bind(this));
        this.crawl_percent_interval = setInterval(
            () => {
                load('crawl_percent', this.populateCrawlPercent.bind(this));
            },
            3600000
        );

        load('articles_saved', this.populateArticlesSaved.bind(this));
        this.articles_saved_interval = setInterval(
            () => {
                load('articles_saved', this.populateArticlesSaved.bind(this));
            },
            3600000
        );

        load('articles_saved2', this.populateArticlesSaved2.bind(this));
        this.articles_saved2_interval = setInterval(
            () => {
                load('articles_saved2', this.populateArticlesSaved2.bind(this));
            },
            3600000
        );
    }



    destroy()
    {
        clearInterval(this.crawl_percent_interval);
        clearInterval(this.articles_saved_interval);
        clearInterval(this.articles_saved2_interval);
    }



    populateArticlesSaved2(articles_saved)
    {
        var old_chart = Chart.getChart('articles_saved2')
        
        if (old_chart) {
            old_chart.destroy();
        }

        var data = {
            labels: articles_saved['labels'],
            datasets: articles_saved['datasets']
        };

        var config = {
            type: 'bar',
            data: data,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                    title: {
                        display: true,
                        text: "Gespeicherte Artikel (letzte 24h)",
                        font: {
                            size: 16,
                            weight: 'bold'
                        }
                    }
                }
            }
        };

        new Chart(
            document.getElementById('articles_saved2'),
            config
        );
    }




    populateArticlesSaved(articles_saved)
    {
        var old_chart = Chart.getChart('articles_saved')
        
        if (old_chart) {
            old_chart.destroy();
        }

        // crawl percent
        var data = {
            labels: articles_saved['labels'],
            datasets: articles_saved['datasets']
        };

        var config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                    title: {
                        display: true,
                        text: "Gespeicherte Artikel (stündlich)",
                        font: {
                            size: 16,
                            weight: 'bold'
                        }
                    },
                    legend: {
                        labels: {
                            usePointStyle: true
                        }
                    }
                }
            }
        };

        new Chart(
            document.getElementById('articles_saved'),
            config
        );
    }



    populateCrawlPercent(crawl_percent)
    {
        var old_chart = Chart.getChart('crawl_percent')
        
        if (old_chart) {
            old_chart.destroy();
        }

        // crawl percent
        var data = {
            labels: crawl_percent['labels'],
            datasets: crawl_percent['datasets']
        };

        var config = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    title: {
                        display: true,
                        text: "Gefundene Artikel die gespeichert wurden (in %)",
                        font: {
                            size: 16,
                            weight: 'bold'
                        }
                    },
                    legend: {
                        labels: {
                            usePointStyle: true
                        }
                    }
                }
            }
        };

        new Chart(
            document.getElementById('crawl_percent'),
            config
        );
    }
}