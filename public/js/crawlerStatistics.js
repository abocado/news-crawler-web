class crawlerStatistics
{
    constructor () 
    {
        this.items = [];
        this.links = [];

        load('crawler_statistics', this.populateCrawlerStatistics.bind(this));
        this.crawler_statistics_interval = setInterval(
            () => {
                load('crawler_statistics', this.populateCrawlerStatistics.bind(this));
            },
            600000
        );


        document.getElementById("detail_close").onclick = () => {
            document.getElementById("detail").style.display = "none";
            overlay(false);
        };

        document.getElementById("save_threshold").onclick = this.saveThreshold.bind(this);
    }



    destroy()
    {
        clearInterval(this.crawler_statistics_interval);
    }



    createStatisticEntry(
        parent, 
        name = false, 
        item = false,
        colspan = 2, 
        rowspan = 1
    )
    {
        var tr, td, th, value, source,
            threshold_min, threshold_max, 
            threshold_min_value, threshold_max_value;
        tr = document.createElement("tr");
        parent.appendChild(tr);

        if (!name)
        {
            th = document.createElement("th");
            th.textContent = "Aktualisierung alle 10 Minuten";
            th.colSpan = colspan;
            tr.appendChild(th);
    
            for (source in this.items)
            {
                th = document.createElement("th");
                th.textContent = source;
                tr.appendChild(th);
            }

            return;
        }
    
        if (name.constructor !== Array)
        {
            name = [name];
        }
    
        for (var n of name)
        {
            td = document.createElement("td");
            td.innerHTML = n;
            td.colSpan = colspan;
            tr.appendChild(td);
    
            if (name.length > 1)
            {
                if (n == name[0])
                {
                    td.rowSpan = rowspan;
                    td.classList.add("vertical");
                    td.innerHTML = "<span>"+n+"</span>";
                }
                else
                {
                    td.classList.add("left");
                }
            }
        }
    
        if (!item)
        {
            td.classList.add("bold");
            td.colSpan = 100;
            return;
        }

        for (source in this.items)
        {
            td = document.createElement("td");
            td.textContent = this.items[source][item] || '0';
            tr.appendChild(td);

            if (!this.links[item])
            {
                continue;
            }

            threshold_min = this.links[item]['threshold_min'];
            threshold_max = this.links[item]['threshold_max'];
    
            if (threshold_min || threshold_max)
            {
                value = parseFloat(this.items[source][item]) || 0;
                threshold_min_value = parseFloat(this.items[source][threshold_min]) || 0;
                threshold_max_value = parseFloat(this.items[source][threshold_max]) || 0;
    
                if (threshold_min_value)
                {
                    td.title = '> ' + this.items[source][threshold_min];
                    if (value < threshold_min_value)
                    {
                        td.classList.add('error');
                    }
                }
                
                if (threshold_max_value)
                {
                    if (td.title)
                    {
                        td.title += " oder ";
                    }
                    else 
                    {
                        td.title = "";
                    }

                    td.title += '< ' + this.items[source][threshold_max];
                    if (value > threshold_max_value)
                    {
                        td.classList.add('error');
                    }
                }

                td.dataset.source_name = source;
                td.dataset.source = this.items[source]['source'];
                td.dataset.item = item;
                td.dataset.value = value;
                td.dataset.threshold_min_value = threshold_min_value;
                td.dataset.threshold_max_value = threshold_max_value;

                td.classList.add('editable');

                td.onclick = this.showDetailPopup.bind(this);
            }
        }
    }
    
    

    showDetailPopup(ev)
    {
        var el = ev.target;
        var source_name = el.dataset.source_name;
        var source = el.dataset.source;
        var item = el.dataset.item;
        //var value = el.dataset.value;
        var threshold_min_value = el.dataset.threshold_min_value;
        var threshold_max_value = el.dataset.threshold_max_value;

        overlay(true);

        document.getElementById("results_title").textContent = "";
        document.getElementById("results_table").innerHTML = "";
        
        var detail_window = document.getElementById("detail");
        detail_window.style.display = "inline";

        // set title
        document.getElementById("detail_title").textContent = source_name + " -> " + item;

        // set source name
        document.getElementById("source_name").textContent = source_name;

        // set threshold value
        var threshold_min_name = this.links[item]['threshold_min'];
        var threshold_min_value_el = document.getElementById("threshold_min_value");
        var threshold_min_operator = document.getElementById("min_operator");

        if (threshold_min_name && threshold_min_name !== 'null')
        {
            threshold_min_value_el.value = threshold_min_value;
            threshold_min_value_el.dataset.source = source;
            threshold_min_value_el.dataset.threshold_min_name = threshold_min_name;
            threshold_min_value_el.style.display = "inline";
            threshold_min_operator.style.display = "inline";
        }
        else
        {
            threshold_min_value_el.value = '';
            delete threshold_min_value_el.dataset.threshold_min_name;
            threshold_min_value_el.style.display = "none";
            threshold_min_operator.style.display = "none";
        }
        

        var threshold_max_name = this.links[item]['threshold_max'];
        var threshold_max_value_el = document.getElementById("threshold_max_value");
        var threshold_max_operator = document.getElementById("max_operator");

        if (threshold_max_name && threshold_max_name !== 'null')
        {
            threshold_max_value_el.value = threshold_max_value;
            threshold_max_value_el.dataset.source = source;
            threshold_max_value_el.dataset.threshold_max_name = threshold_max_name;
            threshold_max_value_el.style.display = "inline";
            threshold_max_operator.style.display = "inline";
        }
        else
        {
            threshold_max_value_el.value = '';
            delete threshold_max_value_el.dataset.threshold_max_name;
            threshold_max_value_el.style.display = "none";
            threshold_max_operator.style.display = "none";
        }

        load(
            "detail", 
            this.requestDetails.bind(this), 
            "&source="+source+"&item="+item
        );
    }



    requestDetails(data)
    {
        var row, col, tr, td, a;

        document.getElementById("results_title").textContent = data["name"];

        var table = document.getElementById("results_table");

        for (row in data["data"])
        {
            tr = document.createElement("tr");
            table.appendChild(tr);

            for (col in data['data'][row])
            {
                td = document.createElement("td");

                if (data['data'][row][col] && data['data'][row][col].startsWith("http"))
                {
                    a = document.createElement("a");
                    a.target = "_blank";
                    a.href = data['data'][row][col];
                    a.textContent = data['data'][row][col];
                    td.appendChild(a);
                }
                else 
                {
                    td.textContent = data['data'][row][col];
                }

                tr.appendChild(td);
            }
        }
    }



    saveThreshold(ev)
    {
        var threshold_min_value_el = document.getElementById("threshold_min_value");
        var threshold_min_value = threshold_min_value_el.value;
        var threshold_min_name = threshold_min_value_el.dataset.threshold_min_name;
        var min_source = threshold_min_value_el.dataset.source;

        var threshold_max_value_el = document.getElementById("threshold_max_value");
        var threshold_max_value = threshold_max_value_el.value;
        var threshold_max_name = threshold_max_value_el.dataset.threshold_max_name;
        var max_source = threshold_max_value_el.dataset.source;

        var params = "";

        if (threshold_min_name)
        {
            params +=
                "&source="+min_source+
                "&threshold_min_name="+threshold_min_name+
                "&threshold_min_value="+threshold_min_value;
        }


        if (threshold_max_name)
        {
            params +=
                "&source="+max_source+
                "&threshold_max_name="+threshold_max_name+
                "&threshold_max_value="+threshold_max_value;
        }

        load(
            "change_threshold", 
            (data) => {
                if (data["saved"] == true)
                {
                    // on success reload statistics and close window
                    load('crawler_statistics', this.populateCrawlerStatistics.bind(this));
                    document.getElementById("detail").style.display = "none";
                    overlay(false);
                }
                else
                {
                    alert("Konnte nicht gespeichert werden!");
                }
            }, 
            params
        );
    }



    populateCrawlerStatistics(data)
    {
        this.items = data['items'];
        this.links = data['links'];

        var table, thead, tbody;

        table = document.getElementById("crawler_statistics");

        // reset table
        table.innerHTML = "";

        // head
        thead = document.createElement("thead");
        table.appendChild(thead);
        
        this.createStatisticEntry(thead);

        // body
        tbody = document.createElement("tbody");
        table.appendChild(tbody);

        // hourly
        this.createStatisticEntry(tbody, 
            "Letzte Stunde"
        );

        this.createStatisticEntry(tbody, 
            "Positionen", 
            "hourly_position_count"
        );

        this.createStatisticEntry(tbody, 
            "Artikel", 
            "hourly_article_count"
        );

        this.createStatisticEntry(tbody, 
            "Suchdurchläufe", 
            "hourly_crawl_result_count"
        );


        // daily
        this.createStatisticEntry(tbody, 
            "Letzte 24h"
        );

        this.createStatisticEntry(tbody, 
            "Artikel", 
            "article_count"
        );

        this.createStatisticEntry(tbody, 
            "Änderungen", 
            "modification_count"
        );

        this.createStatisticEntry(tbody, 
            [
                "Meiste Änder.",
                "Artikel"
            ], 
            "highest_article_modification_count",
            1,
            5
        );

        this.createStatisticEntry(tbody, 
            "URL", 
            "highest_url_modification_count",
            1
        );

        this.createStatisticEntry(tbody, 
            "Titel", 
            "highest_title_modification_count",
            1
        );

        this.createStatisticEntry(tbody, 
            "Beschreibung", 
            "highest_description_modification_count",
            1
        );

        this.createStatisticEntry(tbody, 
            "Inhalt", 
            "highest_content_modification_count",
            1
        );

        this.createStatisticEntry(tbody, 
            [
                "&#x2300; Änderungen",
                "Artikel"
            ], 
            "average_article_modifications",
            1,
            5
        );

        this.createStatisticEntry(tbody, 
            "URL", 
            "average_url_modifications",
            1
        );

        this.createStatisticEntry(tbody, 
            "Titel", 
            "average_title_modifications",
            1
        );

        this.createStatisticEntry(tbody, 
            "Beschreibung", 
            "average_description_modifications",
            1
        );

        this.createStatisticEntry(tbody, 
            "Inhalt", 
            "average_content_modifications",
            1
        );

        this.createStatisticEntry(tbody, 
            [
                "Prozent",
                "Änderungen"
            ], 
            "percent_modified",
            1,
            8
        );

        this.createStatisticEntry(tbody, 
            "Autoren", 
            "percent_authors",
            1
        );

        this.createStatisticEntry(tbody, 
            "Kategorien", 
            "percent_categories",
            1
        );

        this.createStatisticEntry(tbody, 
            "Tags", 
            "percent_tags",
            1
        );

        this.createStatisticEntry(tbody, 
            "Inhalt", 
            "percent_contents",
            1
        );

        this.createStatisticEntry(tbody, 
            "Bild URL", 
            "percent_image_urls",
            1
        );

        this.createStatisticEntry(tbody, 
            "Meinung", 
            "percent_opinion",
            1
        );

        this.createStatisticEntry(tbody, 
            "Paywall", 
            "percent_paywall",
            1
        );

        this.createStatisticEntry(tbody, 
            [
                "Autoren",
                "Durchschnittl."
            ], 
            "average_authors",
            1,
            4
        );

        this.createStatisticEntry(tbody, 
            "Meiste", 
            "highest_author_count",
            1
        );

        this.createStatisticEntry(tbody, 
            "Längster Name", 
            "highest_author_name_length",
            1
        );

        this.createStatisticEntry(tbody, 
            "Meiste Wörter", 
            "highest_author_name_wordcount",
            1
        );

        this.createStatisticEntry(tbody, 
            [
                "Tags",
                "Durchschnittl."
            ], 
            "average_tags",
            1,
            4
        );

        this.createStatisticEntry(tbody, 
            "Meiste", 
            "highest_tag_count",
            1
        );

        this.createStatisticEntry(tbody, 
            "Längster Tag", 
            "highest_tag_name_length",
            1
        );

        this.createStatisticEntry(tbody, 
            "Meiste Wörter", 
            "highest_tag_name_wordcount",
            1
        );

        this.createStatisticEntry(tbody, 
            [
                "Kategorien",
                "Durchschnittl."
            ], 
            "average_categories",
            1,
            4
        );

        this.createStatisticEntry(tbody, 
            "Meiste", 
            "highest_category_count",
            1
        );

        this.createStatisticEntry(tbody, 
            "Längster Name", 
            "highest_category_name_length",
            1
        );

        this.createStatisticEntry(tbody, 
            "Meiste Wörter", 
            "highest_category_name_wordcount",
            1
        );
    }
}