class crawlerLog
{
    constructor () 
    {
        this.latest_timestamp = 0;
        this.latest_date = "";

        load("crawler_log", this.populateCrawlerLog.bind(this));
        this.crawler_log_interval = setInterval(() => {
            load("crawler_log", this.populateCrawlerLog.bind(this), "&timestamp="+this.latest_timestamp);
        }, 10000);

        document.getElementById("log_level_select").onchange = (ev) => {
            this.filterLog(1, ev.target.value);
        };

        document.getElementById("log_thread_search").oninput = (ev) => {
            this.filterLog(2, ev.target.value);
        };

        document.getElementById("log_module_search").oninput = (ev) => {
            this.filterLog(3, ev.target.value);
        };

        document.getElementById("log_func_search").oninput = (ev) => {
            this.filterLog(4, ev.target.value);
        };

        document.getElementById("log_msg_search").oninput = (ev) => {
            this.filterLog(6, ev.target.value);
        };
    }



    destroy()
    {
        clearInterval(this.crawler_log_interval);
    }



    refreshFilter()
    {
        this.filterLog(1, document.getElementById("log_level_select").value);
        this.filterLog(2, document.getElementById("log_thread_search").value);
        this.filterLog(3, document.getElementById("log_module_search").value);
        this.filterLog(4, document.getElementById("log_func_search").value);
        this.filterLog(6, document.getElementById("log_msg_search").value);
    }



    filterLog(column, value)
    {
        var div = document.getElementById("crawler_log_div");
        var table = document.getElementById("crawler_log");
        var tBody = table.getElementsByTagName('tbody')[0];
        var row;

        for (row of tBody.childNodes)
        {
            if (
                !value 
                || value.length < 2 
                || !row.childNodes[column] 
                || row.childNodes[column].textContent.toLowerCase().includes(value.toLowerCase())
            )
            {
                row.classList.remove("log_invisible_"+column.toString())
            }
            else 
            {
                row.classList.add("log_invisible_"+column.toString())
            }
        }

        // scroll to bottom
        div.scrollTop = div.scrollHeight;
    }



    populateCrawlerLog(data)
    {
        var div = document.getElementById("crawler_log_div");
        var table = document.getElementById("crawler_log");
        var tBody = table.getElementsByTagName('tbody')[0];

        var levels = {
            0:  "NOTSET",
            10: "DEBUG",
            20: "INFO",
            30: "WARNING",
            40: "ERROR",
            50: "CRITICAL"
        };

        var tr, td, i;

        var bottom = false;

        if (div.scrollTop === (div.scrollHeight - div.offsetHeight))
        {
            bottom = true;
        }

        for (i in data)
        {
            var date_obj = new Date(parseFloat(data[i]["timestamp"]) * 1000);

            var date =
                date_obj.getFullYear() + "-" +
                (date_obj.getMonth() + 1).toString().padStart(2, "0") + "-" +
                date_obj.getDate().toString().padStart(2,"0")

            if (date != this.latest_date)
            {
                this.latest_date = date;

                tr = document.createElement("tr");
                tr.classList.add("date_row")
                tBody.append(tr);

                td = document.createElement("td");
                td.textContent = date;
                tr.appendChild(td);
            }


            var timestamp = 
                date_obj.getHours().toString().padStart(2,"0") + ":" +
                date_obj.getMinutes().toString().padStart(2,"0") + ":" +
                date_obj.getSeconds().toString().padStart(2,"0") + "." +
                date_obj.getMilliseconds().toString().padStart(3,"0");

            tr = document.createElement("tr");
            tBody.append(tr);

            td = document.createElement("td");
            td.textContent = timestamp;
            tr.appendChild(td);

            td = document.createElement("td");
            td.textContent = levels[data[i]["level"]];
            tr.appendChild(td);

            td = document.createElement("td");
            td.textContent = data[i]["thread"];
            tr.appendChild(td);

            td = document.createElement("td");
            td.textContent = data[i]["module"];
            tr.appendChild(td);

            td = document.createElement("td");
            td.textContent = data[i]["func"];
            tr.appendChild(td);

            td = document.createElement("td");
            td.textContent = data[i]["line"];
            tr.appendChild(td);

            td = document.createElement("td");
            td.textContent = data[i]["msg"];
            td.onclick = function(ev) {
                ev.target.style.whiteSpace = 
                    (['', 'nowrap'].includes(ev.target.style.whiteSpace)) 
                    ? 'break-spaces'
                    : 'nowrap';
            }
            tr.appendChild(td);

            if (data[i]["level"] >= 40)
            {
                tr.classList.add("log_error");
            }
            else if (data[i]["level"] >= 30)
            {
                tr.classList.add("log_warning");
            }
            else if (data[i]["level"] >= 20)
            {
                tr.classList.add("log_info");
            }
        }

        if (i)
        {
            // scroll to bottom (newest)
            if (!this.latest_timestamp || bottom)
            {
                div.scrollTop = div.scrollHeight;
            }

            // clean up
            if (this.latest_timestamp)
            {
                while (tBody.childElementCount > 1000)
                {
                    tBody.removeChild(tBody.firstChild);
                }

                this.refreshFilter();
            }

            this.latest_timestamp = data[i]["timestamp"];
        }
    }
}