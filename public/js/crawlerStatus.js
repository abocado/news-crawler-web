class crawlerStatus 
{
    constructor () 
    {
        this.calculated_time = "";

        load("crawler_status", this.populateCrawlerStatus.bind(this));
        this.crawler_status_interval = setInterval(
            () => {
                load("crawler_status", this.populateCrawlerStatus.bind(this)) 
            },
            5000
        );

        this.calculateTime();
        this.calculate_time_interval = setInterval(
            this.calculateTime.bind(this),
            500
        )
    }



    destroy()
    {
        clearInterval(this.crawler_status_interval);
        clearInterval(this.calculate_time_interval);
    }



    calculateTime()
    {
        var time_classes = document.getElementsByClassName("time");
        if (!time_classes || time_classes.length != 1)
        {
            return;
        }

        var time_el = time_classes[0];
        var timestamp = parseFloat(time_el.dataset.timestamp);

        var date_started = new Date(timestamp * 1000);
        var date_now = new Date();

        var delta = Math.abs(date_started.getTime() - date_now.getTime()) / 1000;

        // calculate (and subtract) whole days
        var days = Math.floor(delta / 86400).toString();
        delta -= days * 86400;

        // calculate (and subtract) whole hours
        var hours = (Math.floor(delta / 3600) % 24).toString().padStart(2, "0");
        delta -= hours * 3600;

        // calculate (and subtract) whole minutes
        var minutes = (Math.floor(delta / 60) % 60).toString().padStart(2, "0");
        delta -= minutes * 60;

        // what's left is seconds
        var seconds = Math.floor(delta % 60).toString().padStart(2, "0");  // in theory the modulus is not required

        this.calculated_time = time_el.textContent = days+":"+hours+":"+minutes+":"+seconds;
    }



    populateCrawlerStatus(crawler_status)
    {
        var parent = document.getElementById("crawler_status");

        if (crawler_status['offline'])
        {
            parent.innerHTML = "Crawler offline!";
            return;
        }

        var table = document.createElement("table");
        var tr, td;

        for (var key in crawler_status)
        {
            tr = document.createElement("tr");
            table.appendChild(tr);
            
            td = document.createElement("td");
            td.textContent = key;
            tr.appendChild(td);

            td = document.createElement("td");
            if (key == "Execution Time")
            {
                td.textContent = this.calculated_time;
            }
            else
            {
                td.textContent = crawler_status[key];
            }
            tr.appendChild(td);
        }

        td.dataset.timestamp = crawler_status[key];
        td.classList.add("time");

        parent.innerHTML = "";
        parent.appendChild(table);
    }
}