var user = window.localStorage.getItem('user');
var pw = window.localStorage.getItem('pw');

var crawler_status = false;
var crawler_log = false;
var crawler_statistics = false;
var crawler_graphs = false;

window.onload = () => {
    if (user && pw)
    {
        start();
    }
}


function start()
{
    crawler_status = new crawlerStatus();
    crawler_log = new crawlerLog();
    crawler_statistics = new crawlerStatistics();
    crawler_graphs = new crawlerGraphs();

    document.getElementById("login").style.display = "none";
}



function destroy()
{
    if (crawler_status !== false)
    {
        crawler_status.destroy();
    }
    if (crawler_log !== false)
    {
        crawler_log.destroy();
    }
    if (crawler_statistics !== false)
    {
        crawler_statistics.destroy();
    }
    if (crawler_graphs !== false)
    {
        crawler_graphs.destroy();
    }
}



function login()
{
    user = document.getElementById("user").value;
    pw = document.getElementById("pw").value;

    if (!user || !pw)
    {
        return false;
    }

    destroy();

    window.localStorage.setItem('user', user);
    window.localStorage.setItem('pw', pw);

    start();
}



function load(_name, callback = ()=>{}, params = "")
{
    if (!user || !pw)
    {
        return false;
    }

    xhr(
        "POST",
        "php/ajax.php",

        (responseText) => {
            let data;
            try 
            {
                data = JSON.parse(responseText);
            }
            catch(e)
            {
                return false;
            }
                
            if (data["error"])
            {
                alert(data["error"]);
            }
            else if (data["login"] === false)
            {
                user = false;
                pw = false;

                window.localStorage.setItem('user', user);
                window.localStorage.setItem('pw', pw);

                destroy();
                document.getElementById("login").style.display = "flex";
            }
            else if (data["status"] === true) 
            {
                //overlay.style.display = "none";
                callback(data[_name]);
            }
            else
            {
                callback(data);
            }
        },

        () => {
            //alert("No connection");
            throw new Error('Session ended');
        },

        "user="+user+"&pw="+pw+"&request=" + _name + params
    );
}



function xhr(method, url, success_func = ()=>{}, failure_func = ()=>{}, params = null) 
{
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() 
    {
        if (xhr.readyState == XMLHttpRequest.DONE) 
        {
            if (xhr.status == 200) 
            {
                success_func(xhr.responseText);
            }
            else 
            {
                failure_func();
            }
        }
    };

    xhr.open(method, url, true);
    if (params)
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(params);
}




function overlay(_status) {
    var overlay = document.getElementById('overlay');
    if (_status) {
        overlay.style.display = 'inline';
    }
    else {
        overlay.style.display = 'none';
    }
}