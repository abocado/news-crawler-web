<?php
function sendCrawlerLog($timestamp, $level = '0')
{
    global $mysqli;

    if (!$timestamp)
    {
        $datetime = new DateTime();
        $datetime->modify("-1 day");
        $timestamp = $datetime->getTimestamp();
    }

    $qry = "
        SELECT
            *
        FROM
            log
        WHERE 
            timestamp > ".$timestamp."
            AND
            level > ".$level."
        ORDER BY
            timestamp DESC
        LIMIT 1000;
    ";


    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    $items = [];

    while ($data = $result->fetch_assoc()) 
    {
        $items[] = $data;
    }

    $items = array_reverse($items);

    echo json_encode($items);
}
?>