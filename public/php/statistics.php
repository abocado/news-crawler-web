<?php
function changeThreshold(
    $source, 
    $threshold_min_name, 
    $threshold_min_value,
    $threshold_max_name, 
    $threshold_max_value
)
{
    global $mysqli;

    $update = "";

    if ($threshold_min_name)
    {
        $threshold_min_value = ($threshold_min_value === NULL || $threshold_min_value === "") ? NULL : "'$threshold_min_value'";

        $update = "$threshold_min_name = $threshold_min_value";
    }

    if ($threshold_max_name)
    {
        $threshold_max_value = ($threshold_max_value === NULL || $threshold_min_value === "") ? NULL : "'$threshold_max_value'";

        if ($update) $update .= ',';
        $update .= "$threshold_max_name = $threshold_max_value";
    }

    $qry = "
        UPDATE
            sources
        SET
            $update
        WHERE
            sources.id = $source
    ";


    if (!$result = $mysqli->query($qry) || $mysqli->affected_rows != 1)
    {
        echo json_encode(["error" => "No changes made!"]);
        return False;
    }

    echo json_encode(["saved" => true]);
}




function sendDetail($source, $item)
{
    global $mysqli;

    $min_results = 1;
    $name = $item;

    if ($item == "highest_article_modification_count")
    {
        $name = "Artikel mit den meisten Änderungen";

        $min_results = 2;
    }

    elseif ($item == "highest_url_modification_count")
    {
        $name = "Artikel mit den meisten geänderten URLs";

        $min_results = 2;
    }

    elseif ($item == "highest_title_modification_count")
    {
        $name = "Artikel mit den meisten geänderten Titeln";

        $min_results = 2;
    }

    elseif ($item == "highest_description_modification_count")
    {
        $name = "Artikel mit den meisten geänderten Beschreibungen";

        $min_results = 2;
    }

    elseif ($item == "highest_content_modification_count")
    {
        $name = "Artikel mit den meisten geänderten Inhalten";

        $min_results = 2;
    }

    elseif ($item == "average_article_modifications")
    {
        $name = "Top 10 Artikel mit den meisten Änderungen";
    }

    elseif ($item == "average_url_modifications")
    {
        $name = "Top 10 Artikel mit den meisten geänderten URLs";
    }

    elseif ($item == "average_title_modifications")
    {
        $name = "Top 10 Artikel mit den meisten geänderten Titeln";
    }

    elseif ($item == "average_description_modifications")
    {
        $name = "Top 10 Artikel mit den meisten geänderten Beschreibungen";
    }

    elseif ($item == "average_content_modifications")
    {
        $name = "Top 10 Artikel mit den meisten geänderten Inhalten";
    }

    elseif ($item == "percent_authors")
    {
        $name = "10 Artikel ohne Autor";
    }

    elseif ($item == "percent_categories")
    {
        $name = "10 Artikel ohne Kategorie";
    }

    elseif ($item == "percent_tags")
    {
        $name = "10 Artikel ohne Tags";
    }

    elseif ($item == "percent_contents")
    {
        $name = "10 Artikel ohne Inhalt";
    }

    elseif ($item == "average_authors")
    {
        $name = "Top 10 Artikel mit den meisten Autoren";
    }

    elseif ($item == "average_categories")
    {
        $name = "Top 10 Artikel mit den meisten Kategorien";
    }

    elseif ($item == "average_tags")
    {
        $name = "Top 10 Artikel mit den meisten Tags";
    }

    elseif ($item == "highest_author_count")
    {
        $name = "Artikel mit den meisten Autoren";
    }

    elseif ($item == "highest_category_count")
    {
        $name = "Artikel mit den meisten Kategorien";
    }

    elseif ($item == "highest_tag_count")
    {
        $name = "Artikel mit den meisten Tags";
    }

    elseif ($item == "highest_author_name_length")
    {
        $name = "Top 10 der längsten Autorennamen";
    }

    elseif ($item == "highest_category_name_length")
    {
        $name = "Top 10 der längsten Kategorienamen";
    }

    elseif ($item == "highest_tag_name_length")
    {
        $name = "Top 10 der längsten Tagnamen";
    }

    elseif ($item == "highest_author_name_wordcount")
    {
        $name = "Top 10 der Autoren mit den meisten Wörtern";
    }

    elseif ($item == "highest_category_name_wordcount")
    {
        $name = "Top 10 der Kategorien mit den meisten Wörtern";
    }

    elseif ($item == "highest_tag_name_wordcount")
    {
        $name = "Top 10 der Tags mit den meisten Wörtern";
    }
    else
    {
        json_encode(["error" => "No result found!"]);
        exit();
    }


    $qry = "
        CALL call_procedure_from_item('$item',$source);
    ";


    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    if ($result->num_rows < $min_results)
    {
        return False;
    }

    $items = [];

    while ($data = $result->fetch_assoc()) 
    {
        $items[] = $data;
    }

    echo json_encode(["name" => $name, "data" => $items]);
}




function sendCrawlerStatistics()
{
    global $mysqli;

    $qry = "
        SELECT
            *
        FROM
            statistics
        JOIN
            sources
            ON sources.id = statistics.source;
    ";


    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    $items = [];

    while ($data = $result->fetch_assoc()) 
    {
        $items[$data["name"]] = $data;
        //unset($items[$data["name"]]["name"]);
        //unset($items[$data["name"]]["source"]);
    }

    ksort($items);

    $qry = "
        SELECT
            *
        FROM
            threshold_links;
    ";


    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    $links = [];

    while ($data = $result->fetch_assoc()) 
    {
        $links[$data["item_name"]] = $data;
        unset($links[$data["item_name"]]["item_name"]);
    }

    echo json_encode([
        'items' => $items,
        'links' => $links
    ]);
}
?>