<?php

function sendArticlesSaved2()
{
    global $mysqli;

    $qry = "
        DROP TEMPORARY TABLE IF EXISTS latest_positions;
        CREATE TEMPORARY TABLE
            latest_positions
            (UNIQUE article_id (article_id))
        SELECT
            (COUNT(DISTINCT position_cache.url) - 1) AS url_modifications,
            urls.article_id
        FROM
            position_cache
        JOIN
            urls
            ON urls.id = position_cache.url
        GROUP BY
            urls.article_id;
        
        DROP TEMPORARY TABLE IF EXISTS latest_articles;
        CREATE TEMPORARY TABLE
            latest_articles
            (UNIQUE article_id (id), INDEX source (source))
        SELECT
            *
        FROM
            latest_positions
        JOIN
            articles
            ON latest_positions.article_id = articles.id;
    ";

    if (!$mysqli->multi_query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    while($mysqli->more_results()){
        $mysqli->next_result();
        $mysqli->use_result();
    }


    $qry = "
        SELECT
            articles_modified.count AS count_modified,
            (articles_total.count - articles_new.count) AS count_rereleased,
            articles_new.count AS count_new,
            articles_total.count AS count_total,
            sources.name
        FROM
            sources

        LEFT JOIN  (
            SELECT
                COUNT(DISTINCT latest_articles.id) AS count,
                latest_articles.source
            FROM
                latest_articles
            GROUP BY
                latest_articles.source
        ) AS articles_total
            ON  sources.id = articles_total.source

        LEFT JOIN (
            SELECT
                COUNT(DISTINCT latest_articles.id) AS count,
                latest_articles.source
            FROM
                latest_articles
            WHERE
                latest_articles.date_published >= (NOW() - INTERVAL 24 HOUR)
            GROUP BY
                latest_articles.source
        ) AS articles_new
            ON sources.id = articles_new.source
        
        LEFT JOIN (
            SELECT
                modifications.source,
                SUM(IF(modifications.count > 0, 1, 0)) AS count
            FROM (
                SELECT		
                    latest_articles.source,
                    latest_articles.url_modifications
                    + COUNT(DISTINCT titles.id) 
                    + COUNT(DISTINCT descriptions.id) 
                    + COUNT(DISTINCT contents.id) AS count
                FROM
                    latest_articles
                LEFT JOIN
                    titles 
                    ON titles.article_id = latest_articles.id
                    AND titles.date >= (NOW() - INTERVAL 24 HOUR)
                LEFT JOIN
                    descriptions 
                    ON descriptions.article_id = latest_articles.id
                    AND descriptions.date >= (NOW() - INTERVAL 24 HOUR)
                LEFT JOIN
                    contents 
                    ON contents.article_id = latest_articles.id
                    AND contents.date >= (NOW() - INTERVAL 24 HOUR)
                GROUP BY
                    latest_articles.article_id
            ) AS modifications
            GROUP BY
                modifications.source
        ) AS articles_modified
            ON sources.id = articles_modified.source

        GROUP BY
            sources.id
        ORDER BY
            sources.name;
    ";

    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    $labels = [];
    $items = [
        "modified" => [],
        "rereleased" => [],
        "new" => []
    ];

    while ($data = $result->fetch_object()) 
    {
        $labels[] = $data->name;

        $items['modified'][] = $data->count_modified;
        $items['rereleased'][] = $data->count_rereleased;
        $items['new'][] = $data->count_new;
    }


    echo json_encode([
        "error"     => False,
        "status"    => True,
        "articles_saved2"  => [
            "datasets"  => [
                [
                    'label' => "New",
                    'data' => $items['new']
                ],
                [
                    'label' => "Modified",
                    'data' => $items['modified']
                ],
                [
                    'label' => "Rereleased",
                    'data' => $items['rereleased']
                ]
                
            ],
            "labels" => $labels
        ]
    ]);
}



function sendArticlesSaved()
{
    global $mysqli;

    $qry = "
        SELECT
            _articles._count,
            sources.name,
            HOUR(_articles.first_seen) AS _hour,
            sources.id
        FROM
            sources
        LEFT JOIN (
            SELECT
                COUNT(articles.id) AS _count,
                articles.first_seen AS first_seen,
                articles.source
            FROM
                articles
            WHERE
                articles.first_seen >= (NOW() - INTERVAL 23 HOUR)
            GROUP BY
                articles.source,
                HOUR(articles.first_seen)
        ) as _articles 
            ON sources.id = _articles.source
        GROUP BY
            sources.id,
            DATE(_articles.first_seen),
            HOUR(_articles.first_seen);
    ";

    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    $results = [];
    $names = [];

    while ($data = $result->fetch_object()) 
    {
        $hour = $data->_hour;
        if (strlen($hour) == 1)
        {
            $hour = "0$hour";
        }

        $name = $data->name;
        
        if (!isset($results[$hour]))
        {
            $results[$hour] = [];
        }

        if (!in_array($name, $names))
        {
            $names[] = $name;
        }

        if (!isset($results[$hour][$name]))
        {
            $count = $data->_count;
            if (!$count)
            {
                $count = 0;
            }

            $results[$hour][$name] = $count;
        }
    }

    natcasesort($names);
    $hours = getHours();

    $datasets = [];

    foreach ($names as $name)
    {
        $dataset = [
            'label' => $name,
            'tension' => 0.3,
            'data' => []
        ];

        foreach ($hours as $hour)
        {
            if (isset($results[$hour]) && isset($results[$hour][$name]))
            {
                $dataset['data'][] = $results[$hour][$name];
            }
            else
            {
                $dataset['data'][] = null;
            }
        }

        $datasets[] = $dataset;
    }

    $string_hours = [];
    foreach ($hours as $hour)
    {
        $string_hours[] = "$hour:00";
    }

    echo json_encode([
        "error"     => False,
        "status"    => True,
        "articles_saved"  => [
            "datasets"  => $datasets,
            "labels"    => $string_hours
        ]
    ]);
}



function sendCrawlPercent()
{
    global $mysqli;

    $qry = "
        SELECT
            sources.name,
            HOUR(results.date) AS hour,
            results.articles_saved,
            results.articles_found
        FROM
            sources
        JOIN (
            SELECT
                crawl_results.date as date,
                crawl_results.source,
                crawl_results.articles_saved,
                crawl_results.articles_found
            FROM
                crawl_results
            WHERE
                crawl_results.date >= (NOW() - INTERVAL 23 HOUR)
            GROUP BY 
                DATE(crawl_results.date), 
                HOUR(crawl_results.date),
                crawl_results.source
        ) AS results ON sources.id = results.source;
    ";

    if (!$result = $mysqli->query($qry))
    {
        echo json_encode(["error" => "No result found!"]);
        return False;
    }

    $results = [];
    $names = [];

    while ($data = $result->fetch_object()) 
    {
        $hour = $data->hour;
        if (strlen($hour) == 1)
        {
            $hour = "0$hour";
        }

        $name = $data->name;
        
        if (!isset($results[$hour]))
        {
            $results[$hour] = [];
        }

        if (!in_array($name, $names))
        {
            $names[] = $name;
        }

        if (!isset($results[$hour][$name]))
        {
            if ($data->articles_saved == 0 && $data->articles_found == 0) 
            {
                $percent = 100;
            }
            elseif ($data->articles_saved == 0 || $data->articles_found == 0)
            {
                $percent = 0;
            }
            else
            {
                $percent = round($data->articles_saved / $data->articles_found * 100, 2);
            }

            $results[$hour][$name] = $percent;
        }
    }

    natcasesort($names);
    $hours = getHours();

    $datasets = [];

    foreach ($names as $name)
    {
        $dataset = [
            'label' => $name,
            'tension' => 0.3,
            'data' => []
        ];

        foreach ($hours as $hour)
        {
            if (isset($results[$hour]) && isset($results[$hour][$name]))
            {
                $dataset['data'][] = $results[$hour][$name];
            }
            else
            {
                $dataset['data'][] = null;
            }
        }

        $datasets[] = $dataset;
    }

    $string_hours = [];
    foreach ($hours as $hour)
    {
        $string_hours[] = "$hour:00";
    }

    echo json_encode([
        "error"     => False,
        "status"    => True,
        "crawl_percent"  => [
            "datasets"  => $datasets,
            "labels"    => $string_hours
        ]
    ]);
}



function getHours()
{
    $date = new DateTime();
    #$date->modify("-1 hour");
    $hour = $date->format("H");

    $hours = [];

    for ($i = 0; $i < 24; $i++)
    {
        if ($hour == 23)
        {
            $hour = -1;
        }

        $hour++;

        if (strlen($hour) == 1)
        {
            $hour = "0$hour";
        }

        $hours[] = "$hour";
    }

    return $hours;
}
?>