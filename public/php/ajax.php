<?php
include "../../cred.php";

$user = $_REQUEST["user"] ?? false;
$pw = $_REQUEST["pw"] ?? false;
if (
    !$user
    || !$pw
    || $user != $dashboard_user
    || !password_verify($pw, $dashboard_pw)
)
{
    sleep(3);
    echo json_encode(['login' => false]);
    exit();
}

$request = $_REQUEST["request"] ?? false;
$source = $_REQUEST["source"] ?? false;
$item = $_REQUEST["item"] ?? false;
$threshold_min_name = $_REQUEST["threshold_min_name"] ?? false;
$threshold_min_value = $_REQUEST["threshold_min_value"] ?? NULL;
$threshold_max_name = $_REQUEST["threshold_max_name"] ?? false;
$threshold_max_value = $_REQUEST["threshold_max_value"] ?? NULL;
$timestamp = $_REQUEST["timestamp"] ?? false;
$level = $_REQUEST["level"] ?? '0';

if ($request == 'crawl_percent')
{
    include "graphs.php";
    sendCrawlPercent();
}

elseif ($request == 'articles_saved')
{
    include "graphs.php";
    sendArticlesSaved();
}

elseif ($request == 'articles_saved2')
{
    include "graphs.php";
    sendArticlesSaved2();
}

elseif ($request == "crawler_status")
{
    sendCrawlerStatus();
}

elseif ($request == "crawler_statistics")
{
    include "statistics.php";
    sendCrawlerStatistics();
}

elseif ($request == 'detail' && $source && $item)
{
    include "statistics.php";
    sendDetail($source, $item);
}

elseif (
    $request == 'change_threshold' 
    && $source
    && (
        $threshold_min_name 
        ||
        $threshold_max_name
    )
)
{
    include "statistics.php";
    changeThreshold(
        $source, 
        $threshold_min_name, 
        $threshold_min_value,
        $threshold_max_name, 
        $threshold_max_value,
    );
}

elseif ($request == "crawler_log")
{
    include "log.php";
    sendCrawlerLog($timestamp, $level);
}




function sendCrawlerStatus()
{
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

    $ctx = stream_context_create(array('http'=>
        array(
            'timeout' => 5,  // 5 seconds
        )
    ));

    $content = file_get_contents("http://localhost:8080", false, $ctx);

    if (!$content)
    {
        echo json_encode(["offline" => true]);
    }
    else
    {
        echo $content;
    }
}
?>